from django.conf.urls import url

from .views import (APIUserList,
                    APIUserDetail,
                    APIVoteFor,)


urlpatterns = [
    url(r'^list$', APIUserList.as_view()),
    url(r'^customer/(?P<pk>[0-9]+)$', APIUserDetail.as_view()),
    url(r'^vote/(?P<pk>[0-9]+)$', APIVoteFor.as_view())
]
