from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers import CustomerSerializer
from customers.models import Customer


class APIUserList(APIView):
    """API для работы со списком пользователей."""

    def get(self, request):
        """Доступ к списку пользоватей."""
        customers = Customer.objects.all()
        serializer = CustomerSerializer(customers, many=True)
        return Response(serializer.data)

    def post(self, request):
        """Создание пользователя."""
        serializer = CustomerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APIUserDetail(APIView):
    """Some text."""

    def get(self, request, pk):
        """Данные конкретного пользователя."""
        customer = get_object_or_404(Customer, pk=pk)
        serializer = CustomerSerializer(customer)
        return Response(serializer.data)

    def delete(self, request, pk):
        """Удаление пользователя."""
        customer = get_object_or_404(Customer, pk=pk)
        customer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class APIVoteFor(APIView):
    def put(self, request, pk):
        """Голосование за определенного пользователя."""
        customer = get_object_or_404(Customer, pk=pk)
        flag = customer.add_vote()
        serializer = CustomerSerializer(customer)
        if flag:
            return Response(serializer.data)
        else:
            Response(serializer.errors, status=status.HTTP_423_LOCKED)
