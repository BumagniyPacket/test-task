from datetime import date

from django.db import models
from django.core.validators import MaxValueValidator


class Customer(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    birthday = models.DateField()
    photo_link = models.URLField(max_length=100)
    votes = models.IntegerField(default=0, validators=[MaxValueValidator(10)])

    @property
    def age(self):
        print(dir(self.birthday))
        days = date.today().year - self.birthday.year
        return days

    def add_vote(self):
        self.votes += 1
        self.save()
