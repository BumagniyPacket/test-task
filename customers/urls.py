from django.conf.urls import url

from .views import (add_customer_view,
                    customer_detail_view,
                    delete_view,
                    export_customers_view,
                    list_view,
                    vote_for,
                    voting_view)


urlpatterns = [
    url(r'^$', list_view, name='list'),
    url(r'^add_customer/$', add_customer_view, name='new_customer'),
    url(r'^(?P<pk>[0-9]+)/$', customer_detail_view, name='detail'),
    url(r'^(?P<pk>[0-9]+)/delete/$', delete_view, name='delete'),
    url(r'^export/$', export_customers_view, name='export_customers'),
    url(r'^voting/$', voting_view, name='voting'),
    url(r'^voting/(?P<pk>[0-9]+)$', vote_for, name='vote_for'),
]
