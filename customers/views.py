from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.csrf import csrf_exempt

from .forms import NewCustomerForm
from .models import Customer
from .utils import create_report


def list_view(request):
    queryset = Customer.objects.all()
    query_name = request.GET.get('q')
    if query_name:
        queryset = queryset.filter(
            Q(name__icontains=query_name) |
            Q(surname__icontains=query_name)
        ).distinct()
    query_order = request.GET.get('ordering')
    if query_order:
        queryset = queryset.order_by(query_order)
    context = {
        'customers': queryset
    }
    return render(request, 'customers/customers_list.html', context=context)


def add_customer_view(request):
    form = NewCustomerForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return redirect('customers:list')
    context = {
        'form': form,
    }
    return render(request, 'customers/customers_new.html', context=context)


def customer_detail_view(request, pk):
    instance = get_object_or_404(Customer, pk=pk)
    context = {
        'customer': instance,
    }
    return render(request, 'customers/customers_detail.html', context=context)


def delete_view(request, pk):
    get_object_or_404(Customer, pk=pk).delete()
    return redirect('customers:list')


def voting_view(request):
    queryset = Customer.objects.all()
    context = {
        'cards': queryset
    }
    return render(request, 'customers/voting.html', context=context)


def vote_for(request, pk):
    get_object_or_404(Customer, pk=pk).add_vote()
    return redirect('customers:voting')


@csrf_exempt
def export_customers_view(request):
    response = create_report()
    return response
