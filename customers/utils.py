from django.shortcuts import HttpResponse

import xlwt

from .models import Customer


def create_report():
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="users.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['id', 'Имя', 'Фамилия', 'Дата рождения', 'Фото']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    row_items = ['pk', 'name', 'surname', 'birthday', 'photo_link']
    rows = Customer.objects.all().values_list(*row_items)
    for num, row in enumerate(rows, start=1):
        for col_num in range(len(row)):
            ws.write(num, col_num, row[col_num], font_style)
    wb.save(response)

    return response
