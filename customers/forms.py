from django import forms

from .models import Customer


class NewCustomerForm(forms.ModelForm):
    birthday = forms.DateField(widget=forms.TextInput(
        attrs={'placeholder': 'YYYY-MM-DD', }
    ))

    class Meta:
        model = Customer
        fields = ('name',
                  'surname',
                  'birthday',
                  'photo_link',)
